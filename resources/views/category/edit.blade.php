@extends('shared._layout')
@section('title','Edit Category')
@section('custom-css')
@endsection
@section('content')
<div style="background-color:white;color:black" class="container">
        <div style="background-color:white; margin-left:300px" class="card card-register mx-auto mt-5">
            <div class="card-header" style="font-size:30px">
                <h1 class="page-header">
         <small>Edit Category: {{$category->name}}</small>
                </h1>
            </div>
          <div class="card-body">
            {{-- @if(count($errors) > 0)
              <div class="alert alert-danger">
                @foreach ($errors ->all() as $err)
                    {{$err}}<br>
                @endforeach
              </div>
              @endif --}}

            @if(session('thongbao'))
              <div class="alert alert-danger">
                {{session('thongbao')}}
              </div>
            @endif
            
            <form action="category/edit/{{$category->id}}" method="POST">
          <input type="hidden" name="_token" value="{{csrf_token()}}"/>
              <div class="form-group">
                <label>Name Catelogy</label>
                    <input class="form-control" name="name" type="text" aria-describedby="nameHelp" placeholder="Enter Name Catelogy">
              </div>
            <div class="form-group">
                <label>Description</label>
                <textarea class="form-control ckeditor" rows="3" id="demo" name="description"> </textarea> 
              </div>
              
              {{-- <a class="btn btn-primary btn-block" href="category/list">Thêm</a> --}}
              <button type="submit" class="form-control btn btn-default" style="background-color:darkgreen; color:white;float:right">Thêm</button>
            </form>
            {{-- <div class="text-center">
              <a class="d-block small mt-3" href="login.html">Login Page</a>
              <a class="d-block small" href="forgot-password.html">Forgot Password?</a>
            </div> --}}
          </div>
        </div>
      </div>
      
@endsection
@section('custom-script')
<script type="text/javascript" language="javascript" src="ckeditor/ckeditor.js">
  
</script>
<script>
        $(document).ready(function(){
            $("#changePassword").change(function(){
                if($(this).is(":checked"))
                {
                    $(".password").removeAttr('disabled');
                }
                else
                {
                    $(".password").attr('disabled','');
                }
            });
        });
    </script>
@endsection
