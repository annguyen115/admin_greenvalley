@extends('shared._layout')
@section('title','Edit Product')
@section('custom-css')
@endsection
@section('content')
<div style="background-color:white;color:black" class="container">
        <div style="background-color:white; margin-left:300px" class="card card-register mx-auto mt-5">
            <div class="card-header" style="font-size:30px">
                <h1 class="page-header">
                  <small>Edit Product: {{$product->name}}</small>
                </h1>
            </div>
          <div class="card-body">
            {{-- @if(count($errors) > 0)
              <div class="alert alert-danger">
                @foreach ($errors ->all() as $err)
                    {{$err}}<br>
                @endforeach
              </div>
              @endif --}}

            @if(session('thongbao'))
              <div class="alert alert-danger">
                {{session('thongbao')}}
              </div>
            @endif
            
            <form action="product/edit/{{$product->id}}" method="POST">
              @csrf
              <div class="form-group">
                  <div class="form-group">
                    <label>Type</label>
                    <select class="form-control" name="type">
                      @foreach($type as $tp )                 
                        <option value="{{$tp->id}}" {{ $product->type_id == $tp->id ? "selected" : ""}}>{{$tp->name}}</option>

                      @endforeach
                    </select>
              </div>
              <div class="form-group">
                <label>Category</label>
                <select class="form-control" name="category">
                  @foreach($category as $ct )
                    <option value="{{$ct->id}} {{ $product->type_id == $ct->id ? "selected" : ""}}">{{$ct->name}}</option>
                  @endforeach
                </select>
              </div>
              <label>Name Product</label>
                  <input class="form-control" name="name" type="text" aria-describedby="nameHelp" placeholder="Enter Name Product" value="{{$product->name}}"> 
              </div>
              <div class="form-group">
                <label>Price</label>
                <input class="form-control" name="price" type="text" placeholder="Enter Price" value="{{$product->price}}">
              </div>
              <div class="form-group">
                <label>Sale Price</label>
                <input class="form-control" name="saleprice" type="text" placeholder="Enter Sale Price" value="{{$product->sale_price}}">
              </div>
              <div class="form-group">
                <label>Stock</label>
                <input class="form-control" name="stock" type="text" placeholder="Enter Stock" value="{{$product->stock}}">
              </div>
              <div class="form-group">
                <label>Description</label>
                <textarea class="form-control ckeditor" rows="3" id="demo" name="description" value="{{$product->description}}"> </textarea> 
              </div>
              <div class="form-group">
                <label>Purchase</label>
                <input class="form-control" name="purchase" type="text" placeholder="Enter Purchase" value="{{$product->purchase}}">
              </div>
              <div class="form-group">
                <label for="exampleConfirmPassword">Visibility</label> <br>
                <input type="checkbox" name="visibility" class="js-switch" {{ $product->visibility == show ? 'value=true checked' : 'value=false'}} />

                {{-- <label class="radio-inline">
                  <input type="radio" name="visibility" value="show" >Show
                </label>
                <label class="radio-inline">
                  <input type="radio" name="visibility" value="hidden" >Hidden
                </label> --}}
              </div>
              <div class="form-group">
                <label>Image</label>
                <input class="form-control" name="image" type="file" value="{{$product->image}}">
              </div>
              <button type="submit" class="form-control btn btn-default" style="background-color:darkgreen; color:white;float:right">Thêm</button>
            </form>
            {{-- <div class="text-center">
              <a class="d-block small mt-3" href="login.html">Login Page</a>
              <a class="d-block small" href="forgot-password.html">Forgot Password?</a>
            </div> --}}
          </div>
        </div>
      </div>
      
@endsection
@section('custom-script')
<script src="libs/ckeditor/ckeditor.js"></script>
@endsection
