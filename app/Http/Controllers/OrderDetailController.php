<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\OrderDetail;
use App\Product;

class OrderDetailController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function getOrderDetail(){
        $orderdetails =  OrderDetail::all();
        return view('orderdetail.list',['orderdetails'=>$orderdetails]);
    }
    public function getCreateOrderDetail(){
        $orders = Order::all();
        $products = Product::all();
        return view('orderdetail.create',['orders'=>$orders],['products'=>$products]);
    }
     public function postCreateOrderDetail(Request $request){         
    
       $orderdetails = new OrderDetail;
           $orderdetails->order_id = $request->or;   
         $orderdetails->product_id = $request->pt;
         $orderdetails->quantity = $request->quantity;
     	$orderdetails->price = $request->price;
    
          $orderdetails->save();

            return redirect('orderdetail/create')->with('thongbao','Thêm orderdetails thành công');
    }
     public function getEditOrderDetail($id){
           $products = Product::all();
         $orders = Order::all();   
        $orderdetails = OrderDetail::find($id);
         return view('orderdetail.edit',['orderdetails'=>$orderdetails],['products'=>$products,'orders'=>$orders]);
    }
      public function postEditOrderDetail(Request $request, $id ){
               $orderdetails = OrderDetail::find($id);
              $orderdetails->order_id = $request->or;   
         $orderdetails->product_id = $request->pt;
         $orderdetails->quantity = $request->quantity;
        $orderdetails->price = $request->price;
    
          $orderdetails->save();

            return redirect('orderdetail/edit/'.$id)->with('thongbao','Đã sửa orderdetail thành công');
    }
      public function getDeleteOrderDetail($id)
    {
        $orderdetails = OrderDetail::find($id);
        $orderdetails -> delete();
        return redirect('orderdetail/list');
    }
}
