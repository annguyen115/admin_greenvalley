@extends('shared._layout')
@section('title','Order List Detail')
@section('custom-css')
<style>
  .card{
    background-color:white; 
    /* color:black; */
    padding: 10px 20px;
  }
  .btn-action{
    text-align: center;
    font-size: 16px;
  
  }
</style>
@endsection
@section('content')
<div style="background-color:white;" class="card mb-3">
    <div class="card-header">
      <div style="font-size:20px"><i class="fa fa-table"></i> List Order Detail <div style="float:right"><a href="orderdetail/create">Create</a></div></div>
    </div>
    <div class="card-body">
      <div  class="table-responsive">
        <table  class="table table-bordered" id="datatable-responsive" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>Id</th>
              <th>Order</th>
              <th>Product</th>
              <th>Quantity</th>
              <th>Price</th>
              <th>Remove</th>
              <th>Update</th>
            </tr>
          </thead>
          <tbody>
            @foreach($orderdetails as $ors)
            <tr>
              <td>{{ $ors->id}}</td>
              <td>{{ $ors->order_id}}</td>
              <td>{{ $ors->product->name}}</td>
              <td>{{ $ors->quantity}}</td>
              <td>{{ $ors->price}}</td>
          
              <td class="btn-action"><a href="orderdetail/delete/{{$ors->id}}"><i class="fa fa-trash fa-fw" style="color:red"></i> </a> </td>
              <td class="btn-action"><a href="orderdetail/edit/{{ $ors->id }}"><i class="fa fa-pencil fa-fw" style="color:seagreen"></i> </a> </td>
             </tr>
           @endforeach
           
          </tbody>
        </table>
      </div>
    </div>
    
  </div>
@endsection
@section('custom-script')
@endsection