@extends('shared._layout')
@section('title','List Product')
@section('custom-css')
<style>
    .card{
      background-color:white; 
      /* color:black; */
      padding: 10px 20px;
    }
    .btn-action{
      text-align: center;
      font-size: 16px;
    
    }
</style>
@endsection
@section('content')
{{-- {{var_dump($products)}} --}}
<div style="background-color:white; color:black" class="card mb-3">
    <div class="card-header">
   <div style="font-size:20px"><i class="fa fa-table"></i> List Product </div>
    </div>
    
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="datatable-responsive" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>Id</th>
              <th>Type</th>
              <th>Name</th>
              <th>Price</th>
              <th>Sale Price</th>
              <th>Stock</th>
              <th>Description</th>
              <th>Image</th>
              <th>Purchase</th>
              <th>Visibility</th>
              <th>View</th>  
              <th>Delete</th>
              <th>Update</th>
            </tr>
          </thead>
          <tbody>
          @foreach($products as $product)
            <tr>
              <td>{{ $product->id}}</td>
              <td>{{ $product->type->name}}</td>

              <td>{{ $product->name}}</td>
              <td>{{ $product->price}}</td>
              <td>{{ $product->sale_price}}</td>
              <td>{{ $product->stock}}</td>
              <td>{{ $product->description}}</td>
              <td>{{ $product->image}}</td>
              <td>{{ $product->purchase}}</td>
              <td>{{ $product->visibility}}</td>
              <td>{{ $product->view}}</td>
              <td class="btn-action"><a href="product/delete/{{$product->id}}"><i class="fa fa-trash fa-fw" style="color:red"></i> </a> </td>
              <td class="btn-action"><a href="product/edit/{{$product->id}}"><i class="fa fa-pencil fa-fw" style="color:seagreen"></i> </a> </td>
            </tr>
           @endforeach 
           
          </tbody>
        </table>
      </div>
    </div>
    <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
  </div>
@endsection
@section('custom-script')
@endsection