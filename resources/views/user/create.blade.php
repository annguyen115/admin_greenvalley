@extends('shared._layout')
@section('title','Create User')
@section('custom-css')
@endsection
@section('content')
<div style="background-color:white; color:black" class="container">
        <div style="background-color:white; margin-left:300px" class="card card-register mx-auto mt-5">
          <div class="card-header" style="font-size:30px">Create User</div>
          <div  class="card-body">
            {{-- @if(count($errors) > 0)
              <div class="alert alert-danger">
                @foreach ($errors ->all() as $err)
                    {{$err}}<br>
                @endforeach
              </div>
              @endif --}}

            @if(session('thongbao'))
              <div style="background-color:darkgreen" class="alert alert-danger">
                {{session('thongbao')}}
              </div>
            @endif
            
            <form action="user/create" method="POST">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
              <div class="form-group">
                <label>Username</label>
                    <input class="form-control" name="username" id="exampleInputLastName" type="text" aria-describedby="nameHelp" placeholder="Enter username">
              </div>
              <div class="form-group">
                <label>Mật khẩu</label>
                <input class="form-control" name="password" id="exampleInputPassword1" type="password" placeholder="Password">
              </div>
              <div class="form-group">
                <label>Nhập lại mật khẩu</label>
                <input class="form-control" id="exampleConfirmPassword" type="password" placeholder="Confirm password">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Email</label>
                <input class="form-control" name="email" type="email" aria-describedby="emailHelp" placeholder="Enter email">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Phone</label>
                <input class="form-control" name="phone" type="text" aria-describedby="phoneHelp" placeholder="Enter phone">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Address</label>
                <input class="form-control" name="address" type="text" aria-describedby="addressHelp" placeholder="Enter address">
              </div>
              <div class="form-group">
                  <label for="exampleInputEmail1">Date of Birth</label>
                  <input class="form-control" name="dob1" type="text" id="datepicker" aria-describedby="addressHelp" placeholder="Enter date of birth">
              </div>
              <div class="form-group">
                <label for="exampleConfirmPassword">Active</label> <br>
                <label class="radio-inline">
                  <input type="radio" name="active" value="actived" >Active
                </label>
                <label class="radio-inline">
                  <input type="radio" name="active" value="not_active" > Not Active
                </label>
            </div>
              <div class="form-group">
                  <label for="exampleConfirmPassword">Quyền người dùng</label> <br>
                  <label class="radio-inline">
                    <input type="radio" name="role" value="user" >User
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="role" value="admin" >Admin
                  </label>
              </div>
              
              {{-- <a class="btn btn-primary btn-block" href="user/list">Thêm</a> --}}
              <button type="submit" class="form-control btn btn-default" style="background-color:darkgreen; color:white;float:right">Thêm</button>
            </form>
            {{-- <div class="text-center">
              <a class="d-block small mt-3" href="login.html">Login Page</a>
              <a class="d-block small" href="forgot-password.html">Forgot Password?</a>
            </div> --}}
          </div>
        </div>
      </div>
      
@endsection
@section('custom-script')
<script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
</script>
@endsection