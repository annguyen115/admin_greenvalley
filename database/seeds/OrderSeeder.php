<?php

use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $orders = [ 
                    [ 'user_id'=>1, 'receiver'=>'Many', 'address'=>'many localhost', 'email'=>'many@localhost.com', 'phone'=>'0931437267', ], 
                    [ 'user_id'=>3, 'receiver'=>'Eve', 'address'=>'Eve localhost', 'email'=>'eve@localhost.com', 'phone'=>'0931437267', ] 
                ];
        DB::table('orders')->insert($orders);
    }
}
