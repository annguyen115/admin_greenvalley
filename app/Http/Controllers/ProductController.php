<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Type;
use App\Category;
class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function getProduct(){
        $products = Product::all();
        return view('product.list',['products'=>$products]);
    }
    public function getCreateProduct(){
        $type = Type::all();
        $category = Category::all();
        return view('product.create',['type'=>$type],['category'=>$category]);
    }
     public function postCreateProduct(Request $request){         
    
        $products = new Product;
        $products->type_id = $request->type;   
        $products->name = $request->name;
        $products->price = $request->price;
        $products->sale_price = $request->saleprice;
        $products->stock = $request->stock;      
        $products->description = $request->description;
        $products->visibility = $request->visibility ? 'show' : 'hidden';
        // xử lý hình ảnh
        $products->image = $request->image;
        $products->save();
        // if($request->hasFile('image')){
        //     $file = $request->file('image');
        //     $fileName =$products->id.'-'.$file->getClientOriginalName();
        //     $fileExtension =  strtolower(\File::extension($fileName));
        //     return $fileName;
        // }
        $category = Category::find( $products->category_id = $request->category);
        $products->Category()->attach($category);  
        return redirect()->back()->with('thongbao','Created Product Success');
    }
     public function getEditProduct($id){
     	 $type = Type::all();
         $category = Category::all();
         $product = Product::find($id);
         return view('product.edit',['product'=>$product,'type'=>$type],['category'=>$category]);
    }
      public function postEditProduct(Request $request, $id ){
         $products = Product::find($id);
           
        $products->type_id = $request->type;
    
         $products->name = $request->name;
         $products->price = $request->price;
     	$products->sale_price = $request->saleprice;
         $products->stock = $request->stock;      
         $products->description = $request->description;
          $products->image = $request->image;
               $products->purchase = $request->purchase;
                  $products->visibility = $request->visibility;
            $products->save();

           $category = Category::find( $products->category_id = $request->category);
            $products->Category()->attach($category);  
            return redirect('product/edit/'.$id)->with('thongbao','Đã sửa sản phẩm thành công');
    }
      public function getDeleteProduct($id)
    {
        $product = Product::find($id);
        $product -> delete();
        return redirect('product/list');
    }
}
