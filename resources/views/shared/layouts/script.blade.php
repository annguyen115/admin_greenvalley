
    <!-- jQuery -->
    <script src="libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="libs/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="libs/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="libs/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="libs/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="libs/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="libs/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="libs/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="libs/Flot/jquery.flot.js"></script>
    <script src="libs/Flot/jquery.flot.pie.js"></script>
    <script src="libs/Flot/jquery.flot.time.js"></script>
    <script src="libs/Flot/jquery.flot.stack.js"></script>
    <script src="libs/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="libs/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="libs/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="libs/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="libs/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="libs/jqvmap/dist/jquery.vmap.js"></script>
    <script src="libs/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="libs/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="libs/moment/min/moment.min.js"></script>
    <script src="libs/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="build/js/custom.min.js"></script>


    {{-- create user --}}
    <script src="build/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="build/js/jquery.easing.min.js"></script>

    <script src="build/js/jquery-ui.js"></script>
    <!-- Datatables -->
    <script src="libs/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="libs/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="libs/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="libs/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="libs/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="libs/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="libs/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="libs/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="libs/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="libs/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="libs/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="libs/switchery/dist/switchery.js"></script>
    <script>
        $(document).ready(function () {
            $('.js-switch').on('change', function () {
                var sw = $(this);
                sw.val(sw.prop('checked'));
            });
        });
    </script>