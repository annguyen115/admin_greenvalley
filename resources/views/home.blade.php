@extends('shared._layout')
@section('title','Home')
@section('custom-css')
@endsection
@section('content')
    @php
        $user = Auth::user();
    @endphp
    <div style="min-height: 800px;">
        <h1>Wellcome back, {{ $user->username }}</h1>
    </div>
@endsection
@section('custom-script')
@endsection