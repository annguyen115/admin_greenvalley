<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('{name}','HomeController@Index')->where(['name' => '|home|index'])->name('home');;
Auth::routes();
Route:: group(['prefix'=>'user'], function(){
    Route::get('list', 'UserController@getListUser');

    Route::get('create', 'UserController@getCreateUser');
    Route::post('create', 'UserController@postCreateUser');

    Route::get('edit/{id}', 'UserController@getEdit');
    Route::post('edit/{id}', 'UserController@postEdit');

    Route::get('delete/{id}', 'UserController@getDelete');
    
});

Route:: group(['prefix'=>'category'], function(){
    Route::get('list', 'CategoriesController@getListCategory');

    Route::get('create', 'CategoriesController@getCreateCategory');
    Route::post('create', 'CategoriesController@postCreateCategory');

    Route::get('edit/{id}', 'CategoriesController@getEditCategory');
    Route::post('edit/{id}', 'CategoriesController@postEditCategory');

    Route::get('delete/{id}', 'CategoriesController@getDeleteCategory');
    
});Route:: group(['prefix'=>'product'], function(){
    Route::get('list', 'ProductController@getProduct');

    Route::get('create', 'ProductController@getCreateProduct');
    Route::post('create', 'ProductController@postCreateProduct');

    Route::get('edit/{id}', 'ProductController@getEditProduct');
    Route::post('edit/{id}', 'ProductController@postEditProduct');

    Route::get('delete/{id}', 'ProductController@getDeleteProduct');
    

    
});Route:: group(['prefix'=>'order'], function(){
    Route::get('list', 'OrderController@getOrder');

    Route::get('create', 'OrderController@getCreateOrder');
    Route::post('create', 'OrderController@postCreateOrder');

    Route::get('edit/{id}', 'OrderController@getEditOrder');
    Route::post('edit/{id}', 'OrderController@postEditOrder');

    Route::get('delete/{id}', 'OrderController@getDeleteOrder');
    
});Route:: group(['prefix'=>'orderdetail'], function(){
    Route::get('list', 'OrderDetailController@getOrderDetail');

    Route::get('create', 'OrderDetailController@getCreateOrderDetail');
    Route::post('create', 'OrderDetailController@postCreateOrderDetail');

    Route::get('edit/{id}', 'OrderDetailController@getEditOrderDetail');
    Route::post('edit/{id}', 'OrderDetailController@postEditOrderDetail');

    Route::get('delete/{id}', 'OrderDetailController@getDeleteOrderDetail');
    
});Route:: group(['prefix'=>'type'], function(){
    Route::get('list', 'TypeController@getType');

    Route::get('create', 'TypeController@getCreateType');
    Route::post('create', 'TypeController@postCreateType');

    Route::get('edit/{id}', 'TypeController@getEditType');
    Route::post('edit/{id}', 'TypeController@postEditType');

    Route::get('delete/{id}', 'TypeController@getDeleteType');
    
});

