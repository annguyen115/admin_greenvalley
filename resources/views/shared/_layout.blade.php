<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel= "shortcut icon" href="favicon.ico">
    <title>@yield('title') - Admin GreenValley </title>
    <base href="{{asset('')}}">
    @include('shared.layouts.style')
    @yield('custom-css')
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <!-- menu -->
        @include('shared.layouts.menu')
        <!-- menu -->
        <!-- top navigation -->
        @include('shared.layouts.header')
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            @yield('content')
        </div>
        <!-- /page content -->

        <!-- footer content -->
        @include('shared.layouts.footer')
        <!-- /footer content -->
      </div>
    </div>
    @include('shared.layouts.script')
    
    @yield('custom-script')
    
  </body>
</html>
