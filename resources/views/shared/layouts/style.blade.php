
    <!-- Bootstrap -->
    <link href="libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="libs/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="libs/iCheck/skins/flat/green.css" rel="stylesheet">
    {{-- user/create --}}

    <!-- Custom styles for this template-->
    <link href="libs/bootstrap/dist/css/sb-admin.css" rel="stylesheet">

    <!-- Datatables -->
    <link href="libs/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="libs/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="libs/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="libs/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="libs/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- bootstrap-progressbar -->
    <link href="libs/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="libs/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="libs/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="build/css/custom.min.css" rel="stylesheet">
    {{-- date of birth --}}
    <link rel="stylesheet" href="libs/switchery/dist/switchery.css" />