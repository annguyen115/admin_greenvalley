@extends('shared._layout')
@section('title','Create Product')
@section('custom-css')
@endsection
@section('content')
<div style="background-color:white; color:black" class="container">
        <div style="background-color:white; margin-left:300px" class="card card-register mx-auto mt-5">
          <div class="card-header" style="font-size:30px">Create Product</div>
          <div  class="card-body">
            {{-- @if(count($errors) > 0)
              <div class="alert alert-danger">
                @foreach ($errors ->all() as $err)
                    {{$err}}<br>
                @endforeach
              </div>
              @endif --}}

            @if(session('thongbao'))
              <div style="background-color:darkgreen" class="alert alert-success">
                {{session('thongbao')}}
              </div>
            @endif
            
            <form action="product/create" method="POST" enctype="multipart/form-data">
              @csrf
              <div class="form-group">
                <label>Type</label>
                <select class="form-control" name="type">
                  @foreach($type as $tp )
                    <option value="{{$tp->id}}" >{{$tp->name}}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <label>Category</label>
                <select class="form-control" name="category">
                  @foreach($category as $ct )
                    <option value="{{$ct->id}}">{{$ct->name}}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                  <label>Name Product</label>
                  <input class="form-control" name="name"  type="text" aria-describedby="nameHelp" placeholder="Enter Name Product">
              </div>
              <div class="form-group">
                <label>Price</label>
                <input class="form-control" name="price" type="text" placeholder="Enter Price">
              </div>
              <div class="form-group">
                <label>Sale Price</label>
                <input class="form-control" name="saleprice" type="number" placeholder="Enter Sale Price" value="0" step="0.01">
              </div>
              <div class="form-group">
                <label>Stock</label>
                <input class="form-control" name="stock" type="text" placeholder="Enter Stock">
              </div>
              <div class="form-group">
                <label>Description</label>
                <textarea class="form-control ckeditor" rows="3" id="demo" name="description"> </textarea> 
              </div>
              <div class="form-group">
                <label for="exampleConfirmPassword">Visibility</label> <br>
                <input type="checkbox" name="visibility" class="js-switch" value="false"/>
              </div>
              <div class="form-group">
                <label>Image</label>
                <input class="form-control" name="image" type="file">
              </div>
       
              
              {{-- <a class="btn btn-primary btn-block" href="product/list">Thêm</a> --}}
              <button type="submit" class="form-control btn btn-default" style="background-color:darkgreen; color:white;float:right">Thêm</button>
            </form>
            {{-- <div class="text-center">
              <a class="d-block small mt-3" href="login.html">Login Page</a>
              <a class="d-block small" href="forgot-password.html">Forgot Password?</a>
            </div> --}}
          </div>
        </div>
      </div>
      
@endsection
@section('custom-script')
<script src="libs/ckeditor/ckeditor.js"></script>
@endsection