@extends('shared._layout')
@section('title','List Type')
@section('custom-css')
<style>
  .card{
    background-color:white; 
    /* color:black; */
    padding: 10px 20px;
  }
  .btn-action{
    text-align: center;
    font-size: 16px;
  
  }
</style>
@endsection
@section('content')
{{-- {{var_dump($products)}} --}}
<div style="background-color:white;" class="card mb-3">
    <div class="card-header">
   <div style="font-size:20px"><i class="fa fa-table"></i> List Type<div style="float:right"><a href="type/create">Create</a></div></div>
    </div>
    
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="datatable-responsive" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>Id</th>
              <th>Name</th>
              <th>Description</th>
              <th>Remove</th>
              <th>Update</th>
            </tr>
          </thead>
          <tbody>
            @foreach($types as $type)
            <tr>
             <td>{{ $type->id}}</td>
             <td>{{ $type->name}}</td>
             <td>{{ $type->description}}</td>
             <td class="btn-action"><a href="type/delete/{{$type->id}}"><i class="fa fa-trash fa-fw" style="color:red"></i> </a> </td>
             <td class="btn-action"><a href="type/edit/{{ $type->id }}"><i class="fa fa-pencil fa-fw" style="color:seagreen"></i> </a> </td>
            </tr>
           @endforeach 
           
          </tbody>
        </table>
      </div>
    </div>
    <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
  </div>
@endsection
@section('custom-script')
<script>
    $(document).ready( function () {
      $('#DataTable').DataTable();
    });
</script>
@endsection