<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $table = 'types';
    public function Product(){
        return $this->hasMany('App\Product','type_id','id');
    }
}
