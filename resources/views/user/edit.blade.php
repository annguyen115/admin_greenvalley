@extends('shared._layout')
@section('title','Edit User')
@section('custom-css')
@endsection
@section('content')
<div style="background-color:white;color:black" class="container">
        <div style="background-color:white; margin-left:300px" class="card card-register mx-auto mt-5">
            <div class="card-header" style="font-size:30px">
                <h1 class="page-header">
                    <small>Edit User: {{$user->username}}</small>
                </h1>
            </div>
          <div class="card-body">
            {{-- @if(count($errors) > 0)
              <div class="alert alert-danger">
                @foreach ($errors ->all() as $err)
                    {{$err}}<br>
                @endforeach
              </div>
              @endif --}}
             
            @if(session('thongbao'))
              @if (session('thongbao.success'))
                <div class="alert alert-success">
                  {{session('thongbao.success')}}
                </div>
              @endif
              @if (session('thongbao.fail'))
                <div class="alert alert-danger">
                  {{session('thongbao.fail')}}
                </div>
              @endif
            @endif
            
            <form action="user/edit/{{$user->id}}" method="POST">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
              <div class="form-group">
                <label>Username</label>
              <input class="form-control" name="username" type="text" value="{{$user->username}}" >
              </div>
              <div class="form-group">
                <input type="checkbox" id="changePassword" name="changePassword" id="">
                <label>Đổi mật khẩu</label>
                <input class="form-control password" name="password" type="password" disabled="" >
              </div>
              <div class="form-group">
                <label>Nhập lại mật khẩu</label>
                <input class="form-control password" name="repassword" type="password" disabled="" >
              </div>
              <div class="form-group">
                <label >Email</label>
                 <input class="form-control" name="email" type="email" value="{{$user->email}}" readonly="">{{--//readonly: khong cho phep sua --}}
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Phone</label>
                <input class="form-control" name="phone" type="text" value="{{$user->phone}}" >
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Address</label>
                <input class="form-control" name="address" type="text" value="{{$user->address}}" aria-describedby="addressHelp" placeholder="Enter address">
              </div>
              {{-- <div class="form-group">
                  <label for="exampleInputEmail1">Date of Birth</label>
                  <input class="form-control" name="dob" type="text" id="datepicker" aria-describedby="addressHelp" placeholder="Enter address">
              </div>
              <div class="form-group">
                  <label for="exampleInputEmail1">Create-at</label>
                  <input class="form-control" name="dob" type="text" id="datepicker" aria-describedby="addressHelp" placeholder="Enter address">
              </div>
              <div class="form-group">
                  <label for="exampleInputEmail1">Update-at</label>
                  <input class="form-control" name="dob" type="text" id="datepicker" aria-describedby="addressHelp" placeholder="Enter address">
              </div> --}}
              <div class="form-group">
                <label for="exampleConfirmPassword">Active</label> <br>
                <input type="checkbox" name="active" class="js-switch" {{ $user->active=='actived' ? 'value=true checked' : 'value=false'}} />
                {{-- <label class="radio-inline">
                    <input type="radio" name="active" value="actived" 
                    @if($user->active == 'actived')
                    {{"checked"}}
                    @endif
                    >Active
                </label>
                <label class="radio-inline">
                    <input type="radio" name="active" value="not_active"
                    @if($user->active == 'not_active')
                    {{"checked"}}
                    @endif
                    >Not Active
                </label> --}}
              </div>
              <div class="form-group">
                  <label for="exampleConfirmPassword">Is Admin</label> <br>
                  <input type="checkbox" name="role" class="js-switch" {{ $user->isAdmin ? 'value=true checked' : 'value=false'}} />
                  {{-- <label class="radio-inline">
                    <input type="radio" name="role" value="user" 
                    @if($user->isIsAdmin)
                    {{"checked"}}
                    @endif
                    >User
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="role" value="admin" 
                    @if($user->role == 'admin')
                    {{"checked"}}
                    @endif
                    >Admin
                  </label> --}}
              </div>
              
              {{-- <a class="btn btn-primary btn-block" href="user/list">Thêm</a> --}}
              
              <button  type="submit" class="form-control btn btn-success">Save Change</button>
              
              </form>
            {{-- <div class="text-center">
              <a class="d-block small mt-3" href="login.html">Login Page</a>
              <a class="d-block small" href="forgot-password.html">Forgot Password?</a>
            </div> --}}
          </div>
        </div>
      </div>
      
@endsection
@section('custom-script')
<script>
        $(document).ready(function(){
            $("#changePassword").change(function(){
                if($(this).is(":checked"))
                {
                    $(".password").removeAttr('disabled');
                }
                else
                {
                    $(".password").attr('disabled','');
                }
            });
        });
    </script>
@endsection
