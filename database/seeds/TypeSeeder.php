<?php

use Illuminate\Database\Seeder;

class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [ 
            [ 'name'=>'Hoa Hồng', 'description'=>'Nhiều loại hoa hồng' ], 
            [ 'name'=>'Hoa Ly', 'description'=>'' ], 
            [ 'name'=>'Hoa Sen', 'description'=>'' ], 
            [ 'name'=>'Hoa Hướng Dương', 'description'=>'' ], 
            [ 'name'=>'Hoa Đồng Tiền', 'description'=>'' ], 
            [ 'name'=>'Hoa Mẫu Đơn', 'description'=>'' ], 
            [ 'name'=>'Hoa Tulip', 'description'=>'' ], 
        ];
        DB::table('types')->insert($types);
    }
}
