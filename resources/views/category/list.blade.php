@extends('shared._layout')
@section('title','Categories List')
@section('custom-css')
@endsection
@section('content')
<div style="background-color:white; color:black" class="card mb-3">
    <div class="card-header">
      <div style="font-size:20px"><i class="fa fa-table"></i> List Categories <div style="float:right"><a href="category/create">Create</a></div></div>
    </div>
    <div class="card-body">
      <div  class="table-responsive">
        <table  class="table table-bordered" id="datatable-responsive" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>Id</th>
              <th>Name</th>
              <th>Description</th>
              <th>Created At</th>
              <th>Updated At</th>
              <th>Delete</th>
              <th>Edit</th>
            </tr>
          </thead>
          <tbody>
            @foreach($categories as $cate)
            <tr>
              <td>{{ $cate->id}}</td>
              <td>{{ $cate->name}}</td>
              <td>{{ $cate->description}}</td>
              <td>{{ $cate->created_at}}</td>
              <td>{{ $cate->updated_at}}</td>
            <td class="center"> <i class="fa fa-trash fa-fw" style="color:red"></i> <a href="category/delete/{{$cate->id}}">Xóa</a> </td>
            <td class="center"> <i class="fa fa-pencil fa-fw" style="color:seagreen"></i> <a href="category/edit/{{$cate->id}}">Sửa</a> </td>
            </tr>
           @endforeach
           
          </tbody>
        </table>
      </div>
    </div>
    
  </div>
@endsection
@section('custom-script')
@endsection