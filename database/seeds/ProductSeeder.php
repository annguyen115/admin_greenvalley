<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = [ 
            ['type_id'=>1, 'name'=>'Hồng Nhung', 'price'=>60, 'stock'=>20, 'description'=>'Tên khoa học: Rose sp.Thuộc họ hoa hồng: Rosaceae-Hoa hồng Đà Lạt là những cây hoa hồng nhung – hoa lớn có các màu: đỏ, trắng, hồng, cam, vàng,… Thường sử dụng trồng vào chậu đặt trong sân vườn hoặc trồng bồn hoa nơi có không khí mát dịu', 'image'=>'maudon.jpg', 'visibility'=>'show' ], 
            ['type_id'=>7, 'name'=>'Hoa Mẫu Đơn', 'price'=>50, 'stock'=>30, 'description'=>'Theo truyền thuyết về các loài hoa thì hoa mẫu đơn ( Trung Quốc) Là mẹ của các loài hoa, khi hoa mẫu đơn nở khoe sắc thì các loài hoa khác không được phép nở. Vẻ đẹp kiêu sa, lộng lẫy của hoa mẫu đơn cùng hương thơm dịu dàng quyến rũ khiến nó trở thành một loài hoa nổi tiếng thế giới. Ngày xưa, hoa mẫu đơn rực rỡ chỉ được cài lên mũ, lên tóc của các hoàng hậu thời xưa tượng trưng cho sự phú quý, thịnh vượng, giàu sang và quyền lực. Ngày nay với công nghệ hiện đại, cây hoa mẫu đơn đã phổ biến hơn và được trồng ở nhiều nơi. Bài viết này chúng ta cùng tìm hiểu đặc điểm và cách trồng giống hoa tuyệt đẹp này bạn nhé!', 'image'=>'maudon.jpg', 'visibility'=>'show' ], 
            ['type_id'=>5, 'name'=>'Hoa Hướng Dương', 'price'=>40, 'stock'=>5, 'description'=>'Hoa hướng dương là loài hoa biểu tượng của mặt trời của hi vọng, của sự ấm áp. Loài hoa luôn hướng về mặt trời, hướng về tương lai, hướng về những điều tốt đẹp nhất. Hiện nay có khá nhiều người yêu thích loại hoa xinh đẹp này, trồng trong nhà như một cây trang trí tô điểm không gian. Những bông hoa nở to màu vàng chói lóa đã làm cho ngôi nhà thêm bừng sáng. Hoa hướng dương bây giờ không chỉ nở vào mùa hè mà người ta đã biết cách trồng làm sao cho hoa nở được cả vào mùa đông lạnh giá nữa, hoa như mang thêm sự ấm áp sưởi ấm những ngày đông.', 'image'=>'maudon.jpg', 'visibility'=>'show' ], 
            ['type_id'=>5, 'name'=>'Hoa ABC', 'price'=>40, 'stock'=>5, 'description'=>'Hoa hướng dương là loài hoa biểu tượng của mặt trời của hi vọng, của sự ấm áp. Loài hoa luôn hướng về mặt trời, hướng về tương lai, hướng về những điều tốt đẹp nhất. Hiện nay có khá nhiều người yêu thích loại hoa xinh đẹp này, trồng trong nhà như một cây trang trí tô điểm không gian. Những bông hoa nở to màu vàng chói lóa đã làm cho ngôi nhà thêm bừng sáng. Hoa hướng dương bây giờ không chỉ nở vào mùa hè mà người ta đã biết cách trồng làm sao cho hoa nở được cả vào mùa đông lạnh giá nữa, hoa như mang thêm sự ấm áp sưởi ấm những ngày đông.', 'image'=>'maudon.jpg', 'visibility'=>'show' ], 
            ['type_id'=>5, 'name'=>'Hoa XYZ', 'price'=>40, 'stock'=>5, 'description'=>'', 'image'=>'maudon.jpg', 'visibility'=>'show' ],
            ['type_id'=>5, 'name'=>'Hoa QWE', 'price'=>40, 'stock'=>5, 'description'=>'', 'image'=>'maudon.jpg', 'visibility'=>'show' ],
            ['type_id'=>5, 'name'=>'Hoa YUI', 'price'=>40, 'stock'=>5, 'description'=>'', 'image'=>'maudon.jpg', 'visibility'=>'show' ],
            ['type_id'=>5, 'name'=>'Hoa UIP', 'price'=>40, 'stock'=>5, 'description'=>'', 'image'=>'maudon.jpg', 'visibility'=>'show' ], 
            ['type_id'=>5, 'name'=>'Hoa NBV', 'price'=>40, 'stock'=>5, 'description'=>'', 'image'=>'maudon.jpg', 'visibility'=>'show' ],
            ['type_id'=>5, 'name'=>'Hoa PLO', 'price'=>40, 'stock'=>5, 'description'=>'', 'image'=>'maudon.jpg', 'visibility'=>'show' ],
            ['type_id'=>5, 'name'=>'Hoa IYR', 'price'=>40, 'stock'=>5, 'description'=>'', 'image'=>'maudon.jpg', 'visibility'=>'show' ], 
            ['type_id'=>5, 'name'=>'Hoa WGB', 'price'=>40, 'stock'=>5, 'description'=>'', 'image'=>'maudon.jpg', 'visibility'=>'show' ], 
            ['type_id'=>5, 'name'=>'Hoa POL', 'price'=>40, 'stock'=>5, 'description'=>'', 'image'=>'maudon.jpg', 'visibility'=>'show' ], 
            ['type_id'=>5, 'name'=>'Hoa AZQ', 'price'=>40, 'stock'=>5, 'description'=>'', 'image'=>'maudon.jpg', 'visibility'=>'show' ], 
            ['type_id'=>5, 'name'=>'Hoa PSD', 'price'=>40, 'stock'=>5, 'description'=>'', 'image'=>'maudon.jpg', 'visibility'=>'show' ], 
            ['type_id'=>5, 'name'=>'Hoa WEA', 'price'=>40, 'stock'=>5, 'description'=>'', 'image'=>'maudon.jpg', 'visibility'=>'show' ], 
            ['type_id'=>5, 'name'=>'Hoa MNY', 'price'=>40, 'stock'=>5, 'description'=>'', 'image'=>'maudon.jpg', 'visibility'=>'show' ], 
            ['type_id'=>5, 'name'=>'Hoa LOK', 'price'=>40, 'stock'=>5, 'description'=>'', 'image'=>'maudon.jpg', 'visibility'=>'show' ], 
            ['type_id'=>5, 'name'=>'Hoa AWE', 'price'=>40, 'stock'=>5, 'description'=>'', 'image'=>'maudon.jpg', 'visibility'=>'show' ], 
            ['type_id'=>5, 'name'=>'Hoa IKS', 'price'=>40, 'stock'=>5, 'description'=>'', 'image'=>'maudon.jpg', 'visibility'=>'show' ], 
            ['type_id'=>5, 'name'=>'Hoa LKJ', 'price'=>40, 'stock'=>5, 'description'=>'', 'image'=>'maudon.jpg', 'visibility'=>'show' ], 
            ['type_id'=>5, 'name'=>'Hoa OKI', 'price'=>40, 'stock'=>5, 'description'=>'', 'image'=>'maudon.jpg', 'visibility'=>'show' ], 
            ['type_id'=>5, 'name'=>'Hoa TYH', 'price'=>40, 'stock'=>5, 'description'=>'', 'image'=>'maudon.jpg', 'visibility'=>'show' ], 
            ['type_id'=>5, 'name'=>'Hoa ACS', 'price'=>40, 'stock'=>5, 'description'=>'', 'image'=>'maudon.jpg', 'visibility'=>'show' ], 
            ['type_id'=>5, 'name'=>'Hoa THG', 'price'=>40, 'stock'=>5, 'description'=>'', 'image'=>'maudon.jpg', 'visibility'=>'show' ], 
            ['type_id'=>5, 'name'=>'Hoa MNV', 'price'=>40, 'stock'=>5, 'description'=>'', 'image'=>'MANY03.jpg', 'visibility'=>'show' ], 
            ['type_id'=>5, 'name'=>'Hoa ZXC', 'price'=>40, 'stock'=>5, 'description'=>'', 'image'=>'MANY02.jpg', 'visibility'=>'show' ], 
            ['type_id'=>5, 'name'=>'Hoa AWS', 'price'=>40, 'stock'=>5, 'description'=>'', 'image'=>'hongnhung.jpg', 'visibility'=>'show' ], 
            
        ];
        DB::table('products')->insert($products);
    }
}
