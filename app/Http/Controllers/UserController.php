<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function getListUser(){
        $users = User::all();
        return view('user.list',['users'=>$users]);
        // $user = UserServices::getAllUser();
        // return var_dump($user);
    }
    public function getCreateUser(){
        return view('user.create');
    }
    public function postCreateUser(Request $request){
            // $this -> validate($request, 
            // [
            //     'username'=>'requested|min:3',
            //     'password'=>'requested|min:6',
            //     'repassword'=>'requested|same:password',
            //     'email'=>'requested|email|unique:users,email',
            //     'phone'=>'requested',
            //     'address'=>'requested',
            //     'active'=>'requested',
            //     'role'=>'requested',
            
            // ],
            // [
            //   'username.requested'=> 'Vui lòng nhập username', 
            //   'username.min'=> 'Vui lòng nhập username với ít nhất 3 kí tự', 
            //   'password.requested'=> 'Vui lòng nhập password' ,
            //   'password.min'=> 'Vui lòng nhập password với ít nhất 8 kí tự', 
            //   'repassword.requested'=> 'Vui lòng nhập repassword' , 
            //   'repassword.same'=> 'Password và Repassword không khớp' , 
            //   'email.requested'=> 'Vui lòng nhập email' ,
            //   'email.email'=> 'Email của bạn không đúng định dạng' ,
            //   'email.unique'=> 'Email đã được đăng ký' ,
            //   'phone.requested'=> 'Vui lòng nhập số điện thoại' ,
            //   'address.requested'=> 'Vui lòng nhập địa chỉ' ,
            //   'active.requested'=> 'Vui lòng nhập active' ,
            //   'role.requested'=> 'Vui lòng nhập số role' ,
            // ]);
            // $validatedData = $request->validate([
            //     'username'=>'requested|min:3',
            // //     'password'=>'requested|min:6',
            // //     'repassword'=>'requested|same:password',
            // //     'email'=>'requested|email|unique:users,email',
            // //     'phone'=>'requested',
            // //     'address'=>'requested',
            // //     'active'=>'requested',
            // //     'role'=>'requested',
            // ]);
            $user = new User;
            $user->username = $request->username;
            $user->password = bcrypt($request->password);
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->address = $request->address;           
            $user->active = $request->active;
            $user->role = $request->role;
            $user->dob = $request->dob1;
            $user->save();
            return redirect('user/create')->with('thongbao','Thêm người dùng thành công');
    }
    public function getEdit($id){
        $user = User::find($id);
        return view('user.edit',['user'=>$user]);
    }
    public function postEdit(Request $request, $id ){
        // $this -> validate($request, 
        //     [
        //         'username'=>'username',
        //         'phone'=>'requested',
        //         'address'=>'requested',
        //         'active'=>'requested',
        //         'role'=>'requested',
            
        //     ],
        //     [
        //       'username.requested'=> 'Vui lòng nhập username', 
        //       'username.min'=> 'Vui lòng nhập username với ít nhất 3 kí tự', 
        //       'phone.requested'=> 'Vui lòng nhập số điện thoại' ,
        //       'address.requested'=> 'Vui lòng nhập địa chỉ' ,
        //       'active.requested'=> 'Vui lòng nhập active' ,
        //       'role.requested'=> 'Vui lòng nhập số role' ,
        //     ]);
            $user = User::find($id);
            $user->username = $request->username;
            $user->phone = $request->phone;
            $user->address = $request->address;           
            $user->active = $request->active? 'active' : 'not_active';
            $user->isAdmin = $request->role? 1:0;
            if($request->changePassword == "on")
            {
                // $this -> validate($request, 
                // [
                //     'password'=>'requested|min:6',
                //     'repassword'=>'requested|same:password'
                
                // ],
                // [
                //   'password.requested'=> 'Vui lòng nhập password' ,
                //   'password.min'=> 'Vui lòng nhập password với ít nhất 8 kí tự', 
                //   'repassword.requested'=> 'Vui lòng nhập repassword' , 
                //   'repassword.same'=> 'Password và Repassword không khớp'
                // ]); 
                if($request->password==$request->repassword)
                    $user->password = bcrypt($request->password);
                else
                    return redirect()->back()->with('thongbao.fail','Password not match with confirm password');

            }
            $user->save();
            return redirect()->back()->with('thongbao.success',"Edited user $user->username successfully");
    }
    public function getDelete($id)
    {
        $user = User::find($id);
        $user -> delete();
        return redirect('user/list');
    }
 }
