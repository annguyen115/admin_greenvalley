<?php

namespace Tests\Unit\Product;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductUnitTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
     /** @test */
 public function it_can_create_a_product()
    {
        $data = [

            'name' => $this->faker->word,
            'price' => $this->faker->word,
            'sale_price' => $this->faker->word,
            'stock' => $this->faker->word,
            'description' => $this->faker->word,
            'image' => $this->faker->url,


        ];
      
        $productRepo = new ProductRepository(new Product);
        $product = $productRepo->createProduct($data);
      
        $this->assertInstanceOf(Product::class, $produc);
        $this->assertEquals($data['name'], $product->name);
        $this->assertEquals($data['price'], $product->price);
         $this->assertEquals($data['sale_price'], $product->sale_price);
        $this->assertEquals($data['stock'], $product->stock);
         $this->assertEquals($data['description'], $product->description);
         $this->assertEquals($data['image'], $product->image);
    }
}
