@extends('shared._layout')
@section('title','Create Order')
@section('custom-css')
@endsection
@section('content')
<div style="background-color:white; color:black" class="container">
        <div style="background-color:white; margin-left:300px" class="card card-register mx-auto mt-5">
          <div class="card-header" style="font-size:30px">Create Order</div>
          <div  class="card-body">
            {{-- @if(count($errors) > 0)
              <div class="alert alert-danger">
                @foreach ($errors ->all() as $err)
                    {{$err}}<br>
                @endforeach
              </div>
              @endif --}}

            @if(session('thongbao'))
              <div style="background-color:darkgreen" class="alert alert-danger">
                {{session('thongbao')}}
              </div>
            @endif
            
            <form action="order/create" method="POST">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    <div class="form-group">
                <label>User</label>
                <select class="form-control" name="user">
                  @foreach($user as $us )
                    <option value="{{$us->id}}">{{$us->username}}</option>
                  @endforeach
                </select>
              </div>

              <div class="form-group">
                <label>Receiver</label>
                <input class="form-control" name="receiver" type="text" placeholder="Enter Name Receiver">
              </div>
              <div class="form-group">
                <label>Address</label>
                <input class="form-control" type="text" name="address" placeholder="Enter Address">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Email</label>
                <input class="form-control" name="email" type="email" aria-describedby="emailHelp" placeholder="Enter Email">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Phone</label>
                <input class="form-control" name="phone" type="text" aria-describedby="phoneHelp" placeholder="Enter Phone Number">
              </div>
               <div class="form-group">
                  <label for="exampleConfirmPassword">Status</label> <br>
                  <label class="radio-inline">
                    <input type="radio" name="status" value="delivery" >Delivery
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="status" value="delivered" >Delivered
                  </label>
              </div>
              <div class="form-group">
                  <label for="exampleInputEmail1">Note</label>
                  <input class="form-control" name="note" type="text" aria-describedby="addressHelp" placeholder="Enter Note">
              </div>
             
              
              {{-- <a class="btn btn-primary btn-block" href="order/list">Thêm</a> --}}
              <button type="submit" class="form-control btn btn-default" style="background-color:darkgreen; color:white;float:right">Thêm</button>
            </form>
            {{-- <div class="text-center">
              <a class="d-block small mt-3" href="login.html">Login Page</a>
              <a class="d-block small" href="forgot-password.html">Forgot Password?</a>
            </div> --}}
          </div>
        </div>
      </div>
      
@endsection
@section('custom-script')
<script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
</script>
@endsection