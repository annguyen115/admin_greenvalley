<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
class CategoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function getListCategory(){
        $categories = Category::all();
        return view('category.list',['categories'=>$categories]);
    }
     public function getCreateCategory(){
        
        return view('category.create');
    }
       public function postCreateCategory(Request $request ){
        
       $category = new Category;
        $category->name = $request->name;
         $category->description = $request->description;
           $category->save();
            return redirect('category/create')->with('thongbao','Thêm thể loại thành công');
    }
     public function getEditCategory($id){
        $category = Category::find($id);
        return view('category.edit',['category'=>$category]);
    }
      public function postEditCategory(Request $request, $id ){
       
            $category = Category::find($id);
            $category->name = $request->name;
            $category->description = $request->description;
            
            $category->save();
            return redirect('category/edit/'.$id)->with('thongbao','Đã sửa thông tin thể loại thành công');
    }
      public function getDeleteCategory($id)
    {
        $category = Category::find($id);
        $category -> delete();
        return redirect('category/list');
    }
}
