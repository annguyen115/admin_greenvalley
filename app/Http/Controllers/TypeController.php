<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Type;

class TypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function getType(){
        $types = Type::all();
        return view('type.list',['types'=>$types]);
      
    }
    public function getCreateType(){
        return view('type.create');
    }
    public function postCreateType(Request $request){
            
            $type = new Type;
            $type->name = $request->name;
            $type->description =$request->description;
         
            $type->save();
            return redirect('type/create')->with('thongbao','Thêm kiểu thành công');
    }
    public function getEditType($id){
        $type = Type::find($id);
        return view('type.edit',['type'=>$type]);
    }
    public function postEditType(Request $request, $id ){
     
            $type = Type::find($id);
              $type->name = $request->name;
            $type->description =$request->description;
         
            $type->save();
            return redirect('type/edit/'.$id)->with('thongbao','Đã sửa thông tin kiểu thành công');
    }
    public function getDeleteType($id)
    {
        $type = Type::find($id);
        $type -> delete();
        return redirect('type/list');
    }
 }
