<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\User;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function getOrder(){
        $orders =  Order::all();
        return view('order.list',['orders'=>$orders]);
    }
    public function getCreateOrder(){
        $user = User::all();
      
        return view('order.create',['user'=>$user]);
    }
     public function postCreateOrder(Request $request){         
    
       $orders = new Order;
           $orders->user_id = $request->user;   
         $orders->receiver = $request->receiver;
         $orders->address = $request->address;
     	$orders->email = $request->email;
         $orders->phone = $request->phone;      
         $orders->status = $request->status;
          $orders->note = $request->note;
          $orders->save();

            return redirect('order/create')->with('thongbao','Thêm order thành công');
    }
     public function getEditOrder($id){
           $user = User::all();
        $order = Order::find($id);
         return view('order.edit',['order'=>$order],['user'=>$user]);
    }
      public function postEditOrder(Request $request, $id ){
               $orders = Order::find($id);
             $orders->user_id = $request->user;   
         $orders->receiver = $request->receiver;
         $orders->address = $request->address;
        $orders->email = $request->email;
         $orders->phone = $request->phone;      
         $orders->status = $request->status;
          $orders->note = $request->note;
          $orders->save();

            return redirect('order/edit/'.$id)->with('thongbao','Đã sửa order thành công');
    }
      public function getDeleteOrder($id)
    {
        $order = Order::find($id);
        $order -> delete();
        return redirect('order/list');
    }
}
