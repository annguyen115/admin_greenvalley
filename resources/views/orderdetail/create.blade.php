@extends('shared._layout')
@section('title','Create Order Detail')
@section('custom-css')
@endsection
@section('content')
<div style="background-color:white; color:black" class="container">
        <div style="background-color:white; margin-left:300px" class="card card-register mx-auto mt-5">
          <div class="card-header" style="font-size:30px">Create Order Detail</div>
          <div  class="card-body">
            {{-- @if(count($errors) > 0)
              <div class="alert alert-danger">
                @foreach ($errors ->all() as $err)
                    {{$err}}<br>
                @endforeach
              </div>
              @endif --}}

            @if(session('thongbao'))
              <div style="background-color:darkgreen" class="alert alert-danger">
                {{session('thongbao')}}
              </div>
            @endif
            
            <form action="orderdetail/create" method="POST">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    <div class="form-group">
                <label>Order</label>
                <select class="form-control" name="or">
                  @foreach($orders as $or)
                    <option value="{{$or->id}}">{{$or->id}}</option>
                  @endforeach
                </select>
              </div>
        <div class="form-group">
                <label>Product</label>
                <select class="form-control" name="pt">
                  @foreach($products as $pt)
                    <option value="{{$pt->id}}">{{$pt->name}}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <label>Quantity</label>
                <input class="form-control" name="quantity" type="text" placeholder="Enter Quantity">
              </div>
              <div class="form-group">
                <label>Price</label>
                <input class="form-control" type="text" name="price" placeholder="Enter Price">
              </div>
             
             
              
              {{-- <a class="btn btn-primary btn-block" href="oderdetail/list">Thêm</a> --}}
              <button type="submit" class="form-control btn btn-default" style="background-color:darkgreen; color:white;float:right">Thêm</button>
            </form>
            {{-- <div class="text-center">
              <a class="d-block small mt-3" href="login.html">Login Page</a>
              <a class="d-block small" href="forgot-password.html">Forgot Password?</a>
            </div> --}}
          </div>
        </div>
      </div>
      
@endsection
@section('custom-script')
<script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
</script>
@endsection