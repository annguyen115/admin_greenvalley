@extends('shared._layout')
@section('title','List User')
@section('custom-css')
  <style>
      .card{
        background-color:white; 
        /* color:black; */
        padding: 10px 20px;
      }
      .btn-action{
        text-align: center;
        font-size: 16px;
      
      }
  </style>
@endsection
@section('content')

<div class="card mb-3">
    <div class="card-header">
      <i class="fa fa-users"></i> List Users
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="datatable-responsive" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>Id</th>
              <th>Username</th>
              <th>Email</th>
              <th>Phone</th>
              <th>Address</th>
              <th>Dob</th>
              <th>Active</th>
              <th>Role</th>
              <th>Create_At</th>
              <th>Update_At</th>
              {{-- <th>Delete</th> --}}
              <th>Edit</th>
            </tr>
          </thead>
          <tbody>
            @foreach($users as $user)
            <tr>
              <td>{{ $user->id}}</td>
              <td>{{ $user->username}}</td>
              {{-- <td>{{ $user->password}}</td> --}}
              <td>{{ $user->email}}</td>
              <td>{{ $user->phone}}</td>
              <td>{{ $user->address}}</td>
              <td>{{ $user->dob}}</td>
              {{-- <td>{{ $user->randomkey}}</td> --}}
              <td>{{ $user->active == 'actived' ? 'Actived' : 'Not Active'}}</td>
              <td>{{ $user->isAdmin ? 'Admin' : 'Member'}}</td>
              <td>{{ $user->created_at}}</td>
              <td>{{ $user->updated_at}}</td>
              {{-- <td class="btn-action" onclick="return confirmBeforeDelete('{{ $user->username}}')"><a href="user/delete/{{$user->id}}"><i class="fa fa-trash fa-fw" style="color:red"></a></td> --}}
              <td class="btn-action"><a href="user/edit/{{$user->id}}"><i class="fa fa-pencil fa-fw" style="color:seagreen"></i> </a> </td>
            </tr>
           @endforeach 
           
          </tbody>
        </table>
      </div>
    </div>
    </div>
@endsection
@section('custom-script')
    <script> 
      function confirmBeforeDelete(name){
          return confirm(`Are you sure you want to remove ${name}??`);
      }
    </script>
@endsection