@extends('shared._layout')
@section('title','Order List')
@section('custom-css')
<style>
  .card{
    background-color:white; 
    /* color:black; */
    padding: 10px 20px;
  }
  .btn-action{
    text-align: center;
    font-size: 16px;
  
  }
</style>
@endsection
@section('content')
<div style="background-color:white; " class="card mb-3">
    <div class="card-header">
      <div style="font-size:20px"><i class="fa fa-table"></i> List Order <div style="float:right"><a href="order/create">Create</a></div></div>
    </div>
    <div class="card-body">
      <div  class="table-responsive">
        <table  class="table table-bordered" id="datatable-responsive" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>Id</th>
              <th>User</th>
              <th>Receiver</th>
              <th>Address</th>
              <th>Email</th>
              <th>Phone</th>
              <th>Status</th>
              <th>Note</th>
              <th>Remove</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            @foreach($orders as $or)
            <tr>
              <td>{{ $or->id}}</td>
              <td>{{ $or->user_id}}</td>
              <td>{{ $or->receiver}}</td>
              <td>{{ $or->address}}</td>
              <td>{{ $or->email}}</td>
              <td>{{ $or->phone}}</td>
              <td>{{ $or->status}}</td>
              <td>{{ $or->note}}</td>
              <td class="btn-action"><a href="order/delete/{{$or->id}}"><i class="fa fa-trash fa-fw" style="color:red"></i> </a> </td>
              <td class="btn-action"><a href="order/edit/{{ $or->id }}"><i class="fa fa-pencil fa-fw" style="color:seagreen"></i> </a> </td>
             </tr>
           @endforeach
           
          </tbody>
        </table>
      </div>
    </div>
    
  </div>
@endsection
@section('custom-script')
@endsection